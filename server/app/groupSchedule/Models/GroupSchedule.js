'use strict';
var Rotor = require('rotor-backbone');

var GroupSchedule = Rotor.Model.extend({
	name: 'groupSchedule',
	defaults: {
            groupName: '',
            keyDates: {},
            weeks: {}
	} 
});

module.exports = GroupSchedule;