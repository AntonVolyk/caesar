'use strict';
var Rotor = require('rotor-backbone');

var LocationSchedule = Rotor.Model.extend({
	name: 'locationSchedule',
	defaults: {
            groupName: '',
            keyDates: {},
            weeks: {}
	} 
});

module.exports = LocationSchedule;