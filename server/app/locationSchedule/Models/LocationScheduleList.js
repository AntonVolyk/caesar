'use strict';

var Rotor = require('rotor-backbone'),
	LocationSchedule = require('./LocationSchedule');

var LocationScheduleList = Rotor.Collection.extend({
	model: LocationSchedule,
    name: 'locationSchedule'
});

module.exports = new LocationScheduleList();