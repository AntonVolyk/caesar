'use strict';
var Rotor = require('rotor-backbone'),
    Controller = Rotor.Controller.extend({
	    collection: require('./Models/LocationScheduleList'),
    });

module.exports = new Controller();