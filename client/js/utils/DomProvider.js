'use strict';
var DomProvider = {
	row: $('#row'),
	topMenu: $('#top-menu'), 
	wrapper: $('#wrapper'),
	header: $('#header'),
	logo: $('#logo'),
	icon: $('#icon'),
	leftMenu:$('#left-menu'),
	rightMenu: $('#right-menu'),
	page: $('#page'),
	leftSideBar: $('#left-side-bar'),
	contentSection: $('#content-section'),
	rightSideBar: $('#right-side-bar'),
	modalWindow: $('#modal-window')
};

