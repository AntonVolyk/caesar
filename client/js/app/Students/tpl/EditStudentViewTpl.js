'use strict';

templates.studentEditTpl =
    `<section class="modal-window modal_singleStudent">
        <section class="form-inline form-wrapper container">

            <div class="row">
                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4">
                    <label class="control-label">Group name</label>
                    <input name="groupId" value="<%- groupId %>" class="form-control" disabled>
                </div>

                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4">
                    <label class="control-label">Incoming test</label>
                    <input name="IncomingTest" value="<%- incomingScore %>" class="form-control incomingTest">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4 firstNameInput">
                    <label class="control-label">First name</label>
                    <input name="FirstName" value="<%- name %>" class="form-control firstName">
                </div>

                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4 firstNameInput">
                    <label class="control-label">Entry score</label>
                    <input name="EntryScore" value="<%- entryScore %>" class="form-control entryScore">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4">
                    <label class="control-label">Last name</label>
                    <input name="LastName" value="<%- lastName %>" class="form-control lastName">
                </div>

                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4">
                    <label class="control-label">Approved by</label>
                    <select class="form-control approvedBy">
                        <option value="Not approved">Not approved</option>
                        <option value=Custom>Custom</option>
                        <option value="Exptert #1">Exptert #1</option>
                        <option value="Exptert #2">Exptert #2</option>
                        <option value="Exptert #3">Exptert #3</option>
                        <option value="Exptert #4">Exptert #4</option>
                        <option value="Exptert #5">Exptert #5</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4">
                    <label class="control-label">English level</label>
                    <select class="form-control englishLevel">
                        <% if (englishLevel === "Elementary" || englishLevel === '') { %>
                            <option value="Elementary" selected>Elementary</option>
                        <% } else { %>
                            <option value="Elementary">Elementary</option>
                        <% } %>
                        <% if (englishLevel === "Pre-intermediate low") { %>
                            <option value="Pre-intermediate low" selected>Pre-intermediate low</option>
                        <% } else { %>
                            <option value="Pre-intermediate low">Pre-intermediate low</option>
                        <% } %>
                        <% if (englishLevel === "Pre-intermediate") { %>
                            <option value="Pre-intermediate" selected>Pre-intermediate</option>
                        <% } else { %>
                            <option value="Pre-intermediate">Pre-intermediate</option>
                        <% } %>
                        <% if (englishLevel === "Pre-intermediate strong") { %>
                            <option value="Pre-intermediate strong" selected>Pre-intermediate strong</option>
                        <% } else { %>
                            <option value="Pre-intermediate strong">Pre-intermediate strong</option>
                        <% } %>
                        <% if (englishLevel === "Intermediate low") { %>
                            <option value="Intermediate low" selected>Intermediate low</option>
                        <% } else { %>
                            <option value="Intermediate low">Intermediate low</option>
                        <% } %>
                        <% if (englishLevel === "Intermediate") { %>
                            <option value="Intermediate" selected>Intermediate</option>
                        <% } else { %>
                            <option value="Intermediate">Intermediate</option>
                        <% } %>
                        <% if (englishLevel === "Intermediate strong") { %>
                            <option value="Intermediate strong" selected>Intermediate strong</option>
                        <% } else { %>
                            <option value="Intermediate strong">Intermediate strong</option>
                        <% } %>
                        <% if (englishLevel === "Upper-intermediate low") { %>
                            <option value="Upper-intermediate low" selected>Upper-intermediate low</option>
                        <% } else { %>
                            <option value="Upper-intermediate low">Upper-intermediate low</option>
                        <% } %>
                        <% if (englishLevel === "Upper-intermediate") { %>
                            <option value="Upper-intermediate" selected>Upper-intermediate</option>
                        <% } else { %>
                            <option value="Upper-intermediate">Upper-intermediate</option>
                        <% } %>
                        <% if (englishLevel === "Upper-intermediate strong") { %>
                            <option value="Upper-intermediate strong" selected>Upper-intermediate strong</option>
                        <% } else { %>
                            <option value="Upper-intermediate strong">Upper-intermediate strong</option>
                        <% } %>
                        <% if (englishLevel === "Advanced") { %>
                            <option value="Advanced" selected>Advanced</option>
                        <% } else { %>
                            <option value="Advanced">Advanced</option>
                        <% } %>
                    </select>
                </div>

                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4">
                    <label class="control-label custom-approval"></label>
                    <input name="CustomApproval" class="form-control custom-approval-input" disabled>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4">
                    <label class="control-label">CV</label>

                    <div class="pull-right">
                        <button class="BrowseCV form-control btn btn-default active">Browse</button>
                        <div class="downloadedCV hidden">
                            <button class="deleteCV"></button>
                        </div>
                    </div>
                </div>

                <div class="form-group col-xs-6 col-xs-offset-0 col-md-5 col-md-offset-1 col-lg-4">
                    <label class="control-label">Photo</label>

                    <div class="pull-right">
                        <button class="BrowsePhoto form-control btn btn-default active">Browse</button>
                        <div class="downloadedPhoto hidden">
                            <button class="deletePhoto"></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="buttons">
                <button class="save-changes fa fa-check-circle-o fa-3x btn-icon"></button>
                <button class="close-modal-window fa fa-times-circle-o fa-3x btn-icon"></button>
            </div>

        </section>
    </section>`;