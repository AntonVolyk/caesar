'use strict';

(function (This) {
	This.StudentsList = Backbone.Collection.extend({
        model: This.Student,
		url: '/students',
        comparator: function (item) {
            return item.get('lastName');
        }
	});
})(CS.Students);