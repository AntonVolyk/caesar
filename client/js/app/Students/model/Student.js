'use strict';

(function (This) {
	This.Student = Backbone.Model.extend({
		defaults: {
			groupId: '',
			name: '',
			lastName: '',
			englishLevel: '',
			CvUrl: '',
			avatar: '',
            incomingScore: '',
            entryScore: '',
			approvedBy: ''
		}
	});
})(CS.Students);