'use strict';

(function (This, app) {
    This.Controller = Backbone.Controller.extend({
        subscribes: {
            'GroupList paginator: page-selected': 'groupsRender',
            'Students: score-request': 'showScoreStudentList',
            'Students: groups selected': 'showSelectedGroup',
            'Students: editStudent request': 'editStudent',
            'Students: create-request': 'createStudent',
            'Students: edit-request': 'showForm',
            'Students: delete-request': 'delete',
            'Menu: changed-page': 'deleteView',
            'Locations: selected': 'show'
        },

        initialize: function () {
            ActivePage.setDescription('Students');

            this.mediator = app.mediator;

            this.$content = $('#content-section');  
            this.$sidebar = $('#left-side-bar');
            this.$main = $('.mainSection');
        },
        
        show: function (locations) {
            var description = ActivePage.getDescription();

            if (description === 'Students'){
                if (this.groupListView) {
                    this.deleteView();    
                }

                this.contentView = new This.ContentView();
                this.$content.html(this.contentView.render().el);
                
                this.groupListView = new CS.GroupList.GroupListView();
                this.$sidebar.html(this.groupListView.render(locations).el);
                
                this.isOpen = true;
                this.locations = locations;
            }    
        },

        showSelectedGroup: function (group, action) {
            this.currentGroup = group;
            this.initCollection();

            var groupView = new This.StudentsView({
                model: group[0],
                collection: this.studentsCollection
            });

            this.$content.find($('.mainSection')).html(groupView.render().el);
            groupView.showStubView(action);
        },

        initCollection: function () {
            this.studentsCollection = app.filter.split('students', this.currentGroup);
        },

        groupsRender: function () {
            if (this.isOpen) {
                this.groupListView.renderGroups(this.locations);
            }
        },

        showForm: function () {
            this.initCollection();
            this.editStudentListView = new This.EditStudentListView({
                collection: this.studentsCollection
            });

            this.modal(this.editStudentListView);
        },

        showScoreStudentList: function () {
            this.scoreModalStudentListView = new This.ScoreModalStudentListView({
                collection: this.studentsCollection
            });

            this.modal(this.scoreModalStudentListView);
        },

        createStudent: function () {
            this.createStudent = new This.CreateStudentView({
                model: this.currentGroup
            });

            this.modal(this.createStudent);

            this.approvalCheck();
        },

        editStudent: function (student) {
            this.editStudent = new This.EditStudentView({
                model: student
            });

            this.modal(this.editStudent);

            this.approvalCheck();
        },


        showGroupViewByRoute: function (locations, groupName, action) {
            if (this.showLocationByRoute(locations)) {
                if (store.groups.findGroupByName(groupName)) {
                    this.showSelectedGroup(this.list(locations).findGroupsByName(groupName), action);
                } else {
                    app.mediator.publish('Error: show-error-page', {
                        elem: this.$main,
                        message: 'such a group is not found'
                    });
                }
            }

            return store.groups.findGroupByName(groupName);
        },

        showLocationByRoute: function (arrLocations) {
            if (isLocation(arrLocations)) {
                app.mediator.publish('Error: show-error-page', {
                    elem:  this.$main,
                    message: 'such a location is not found'
                });

                return false;
            } else {
                app.mediator.publish('Locations: selected', arrLocations);

                return true;
            }

            function isLocation(locations) {
                var arr = [];

                locations.forEach(function (location) {
                    if (store.locations.getNames().indexOf(location) < 0) {
                        arr.push(location);
                    }
                });

                return arr.length;
            }
        },

        deleteView: function () {
            if (this.isOpen) {
                this.isOpen = false;
                this.contentView.remove();
                this.groupListView.remove();
                this.groupListView.paginatorView.remove();
            }
        },

        delete: function (student) {
            var studentDeleteView = new This.StudentDeleteView({
                model: student
            });
            
            this.modal(studentDeleteView);
        },

        modal: function (view) {
            DomProvider.modalWindow.html(view.render().el);

            DomProvider.modalWindow.html(view.render().el);
        },

        approvalCheck: function () {
            var customApproval = "Custom",
                customInput = $('.approvedBy');

            $('.approvedBy').change(function () {
                var customApprovalInput = $('.approvedBy');

                customApprovalInput.prop('disabled', true);

                if($('.approvedBy').val() === customApproval ) {
                    customInput.html('Custom approve');
                    customApprovalInput.prop('disabled', false);
                } else if ($('.approvedBy').val() !== customApproval) {
                    // customApprovalInput.html(''); doesn't clearing input
                    customInput.html('');
                    customApprovalInput.prop('disabled', true);
                }

            });
        },

        list: function (data) {
            return store.groups.findGroupsByLocations(data);
        }
    })
})(CS.Students, app);