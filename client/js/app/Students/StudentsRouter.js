'use strict';

(function (This, app) {
	This.Router = Backbone.Router.extend({
		currentUrl: 'Students',

		routes: {
            '': 'initLocation',
			'Students(/)': 'initLocation',
			'Students/Locations(/)': 'openWindowLocations',
            'Students/:location(/)': 'openLocation',
            'Students/:location/:group(/)': 'openGroupList',
            'Students/:location/:group/:action(/)': 'openGroupAction',
            'Students*path': 'notFound'
		},

		subscribes: {
            'Students: stubView-changed': 'navToGroupAction',
            'Students: groups selected': 'navToGroupSelected'
        },

        initialize: function () {
            app.mediator.multiSubscribe(this.subscribes, this);

            this.controller = new This.Controller();

            Backbone.history.loadUrl(Backbone.history.fragment);
        },

        initLocation: function () {
            var locations = app.locationsController.getSelectedLocations(),
                arrLocations = locations.join('+');

            app.mediator.publish('Locations: selected', locations);

            this.navigate('Students/' + arrLocations);
        },
        
        openWindowLocations: function () {
             app.locationsController.showLocations();
        },

        openGroupList: function (locations, groupName) {
            var arrLocations = locations.split('+'),
                modelGroup = this.controller.showGroupViewByRoute(arrLocations, groupName, 'list');

            groupName.split('+');

            if (modelGroup) {
                this.navigate('Students/' + locations + '/' + groupName + '/list');
            }
        },

        openLocation: function (locations) {
            var arrLocations = locations.split('+');
            this.controller.showLocationByRoute(arrLocations);
        },        

        openGroupAction: function (locations, groupName, action) {
            var arrLocations = locations.split('+'),
                actions = {
                    'list': true
                };

            if (actions[action]) {
                this.controller.showGroupViewByRoute(arrLocations, groupName, action);
            } else {
                this.controller.showGroupViewByRoute(arrLocations, groupName, 'list');
            }
        },

        navToGroupAction: function (args) {
            var groupName = args.group.get('name'),
                location = args.group.get('location'),
                action = args.stubView;

            this.navigate('Students/' + location + '/' + groupName + '/' + action);
        },

        navToGroupSelected: function (models) {
            var groupName, location;

            if (models.length === 1) {
                groupName = models[0].get('name');
            } else {
                groupName = models.map(function (group) {
                    return group.get('name');
                });

                groupName = groupName.join('+');
            }

            location = Backbone.history.fragment.split('/')[1];

            this.navigate('Students/' + location + '/' + groupName + '/list');
        },

        notFound: function () {
            app.mediator.publish('Error: show-page-404');
        }
	});
})(CS.Students, app);