'use strict';

(function (This, app) {
    This.OneStudentView = Backbone.View.extend({
        tagName: 'tbody',

        className: 'tableBodyStudents',

        template: _.template(templates.oneStudentViewTpl),

        events: {
            'click [name="studName"]': 'showStudent'
        },

        render: function () {
            this.$el.append(this.template(this.model.toJSON()));

            return this;
        },

        showStudent: function () {
            // this.showStudent = new This.StudentView({model: this.model});
            // $('#modal-window').html(this.showStudent.render().el);

            alert('I will be showing a student');
        }
    });
})(CS.Students, app);