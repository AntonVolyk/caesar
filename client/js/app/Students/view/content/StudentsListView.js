'use strict';

(function (This, app) {
    This.StudentsListView = Backbone.View.extend({
        tagName: 'table',

        className: 'students_list',

        template: _.template(templates.studentsListViewTpl),

        events: {
            'click .name': 'sortByName',
            'click .engLevel': 'sortByEngLevel'
        },

        initialize: function () {
            store.students.off('add remove change', this.reRender);
            store.students.on('add remove change', this.reRender, this);
            
            this.nameOrder = false;
            this.engOrder = true;
        },

        reRender: function () {
            this.collection = app.filter.split('students', this.model);
            this.render();
        },

        render: function () {
            this.$el.empty();
            this.$el.html(this.template());
            
            this.collection.forEach(function (student) {
                var view = new This.OneStudentView({model: student});
                this.$el.append(view.render().el);
            }, this);

            return this;
        },

        sortByName: function () {
            var attribute = 'lastName';

            if (this.nameOrder) {
                this.collection.sort(myComparator);
            } else {
                this.collection.sort(myComparator).reverse();
            }

            this.render();
            this.nameOrder = !this.nameOrder;
            this.engOrder = true;

            function myComparator (a, b) {
                if (a.get(attribute).toLowerCase() < b.get(attribute).toLowerCase()) {
                    return -1;
                } else if (a.get(attribute).toLowerCase() > b.get(attribute).toLowerCase()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        },

        sortByEngLevel: function () {
            var attribute = 'englishLevel';

            if (!this.engLevels) {
                this.engLevels = {};

                i.englishLevels.forEach(function (item, index) {
                    this.engLevels[item] = index;
                }, this);
            }

            if (this.engOrder) {
                this.collection.sort(myComparator.bind(this));
            } else {
                this.collection.sort(myComparator.bind(this)).reverse();
            }

            this.render();
            this.engOrder = !this.engOrder;
            this.nameOrder = true;

            function myComparator (a, b) {
                if (this.engLevels[a.get(attribute)] < this.engLevels[b.get(attribute)]) {
                    return -1;
                } else if (this.engLevels[a.get(attribute)] > this.engLevels[b.get(attribute)]) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    });
})(CS.Students, app);