'use strict';

(function (This, app) {
    This.OneEditStudentListView = Backbone.View.extend({
        tagName: 'tr',

        template: _.template(templates.studentListModalItemTpl),

        events: {
            'click .deleteStudent': 'deleteStudent',
            'click .editStudent': 'editStudent'
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));

            return this;
        },

        deleteStudent: function () {
            app.mediator.publish('Students: delete-request', this.model);
        },

        editStudent: function () {
            app.mediator.publish('Students: editStudent request', this.model);
        }
    });
})(CS.Students, app);