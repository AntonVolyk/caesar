'use strict';

(function (This, app) {
	This.EditStudentListView = Backbone.View.extend({
		tagName: 'section',

		className: 'backdrop',

		template: _.template(templates.studentListModalTpl),

		events: {
			'click .createStudent': 'createStudent',
			'click .downloadCV': 'downloadCV',
			'click .modal_editStudentlist': 'editStudent',
			'click .right': 'scoreStudentList',
			'click .exit': 'exit',
			'click .name': 'sortByName',
			'click .engLevel': 'sortByEngLevel'
		},
		
		initialize: function () {
			this.mediator = app.mediator;

			this.nameOrder = false;
			this.engOrder = true;
		},

		render: function () {
			this.$el.html(this.template());

			this.collection.forEach(function (student) {
				var view = new This.OneEditStudentListView({model: student});
				this.$el.find('.tableBodyStudents').append(view.render().el);
			}, this);

			$(document).on('keydown', keyEvent.bind(this));
			
			function keyEvent (event) {
				if (event.which === System.constants.ESC) {
					this.exit();
				}
			}

			return this;
		},

		createStudent: function () {
			this.mediator.publish('Students: create-request');
		},

		downloadCV: function () {
			
		},

		scoreStudentList: function () {
			this.mediator.publish('Students: score-request');
		},

		sortByName: function () {
            var attribute = 'lastName';

            if (this.nameOrder) {
                this.collection.sort(myComparator);
            } else {
                this.collection.sort(myComparator).reverse();
            }

            this.render();
            this.nameOrder = !this.nameOrder;
            this.engOrder = true;

            function myComparator (a, b) {
                if (a.get(attribute).toLowerCase() < b.get(attribute).toLowerCase()) {
                    return -1;
                } else if (a.get(attribute).toLowerCase() > b.get(attribute).toLowerCase()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        },

        sortByEngLevel: function () {
            var attribute = 'englishLevel';

            if (!this.engLevels) {
                this.engLevels = {};

                i.englishLevels.forEach(function (item, index) {
                    this.engLevels[item] = index;
                }, this);
            }

            if (this.engOrder) {
                this.collection.sort(myComparator.bind(this));
            } else {
                this.collection.sort(myComparator.bind(this)).reverse();
            }

            this.render();
            this.engOrder = !this.engOrder;
            this.nameOrder = true;

            function myComparator (a, b) {
                if (this.engLevels[a.get(attribute)] < this.engLevels[b.get(attribute)]) {
                    return -1;
                } else if (this.engLevels[a.get(attribute)] > this.engLevels[b.get(attribute)]) {
                    return 1;
                } else {
                    return 0;
                }
            }
        },

		exit: function () {
			$(document).off('keydown');
			$(document).off('click');
			this.remove();
		}
	});
})(CS.Students, app);
