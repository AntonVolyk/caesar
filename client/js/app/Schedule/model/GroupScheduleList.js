'use strict';
(function (This) {
    This.GroupScheduleList = Backbone.Collection.extend({
        model: This.GroupSchedule,
        url: '/groupSchedule'
    });
})(CS.Schedule);
