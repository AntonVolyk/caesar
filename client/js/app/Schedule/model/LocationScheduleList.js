'use strict';
(function (This) {
    This.LocationScheduleList = Backbone.Collection.extend({
        model: This.LocationSchedule,
        url: '/locationSchedule'
    });
})(CS.Schedule);
