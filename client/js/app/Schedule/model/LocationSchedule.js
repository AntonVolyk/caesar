'use strict';

(function (This, i) {
    This.LocationSchedule = Backbone.Model.extend({
        defaults: function () {
            return {
                groupName: '',
                keyDates: {},
                weeks: {}
            };
        }
    });
})(CS.Schedule, i);

