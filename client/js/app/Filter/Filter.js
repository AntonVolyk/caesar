'use strict';
(function (This, app) {
    This.Filter = function () {
        var groupListParams = {
                state: 'in-process',
                isMyGroups: false,
                locations: [],
                page: 0,
                lastPage: 0,
                pageSize: 8
            },
            subscribes = {
                'GroupList paginator: page size defined': onPageChange,
                'GroupList paginator: page-selected': onPageSelected,
                'MyGroups: selected': onMyGroups,
                'State: selected': onStateChange
            };

        app.mediator.multiSubscribe(subscribes, this);
        function onPageSelected (value) {
            groupListParams.page = value;
        }

        function onPageChange (value) {
            groupListParams.pageSize = value;
        }

        function onStateChange (value) {
            groupListParams.state = value;
        }

        function onMyGroups (value) {
            groupListParams.isMyGroups = value;
        }

        this.split = function (collectionName, options) {
            var dataList = {};

            dataList = store[collectionName];

            if (collectionName === 'groups') {
                dataList = dataList.findGroupsByLocations(options);
                dataList = dataList.findGroupsByState(groupListParams.state);

                if (groupListParams.isMyGroups) {
                    dataList = dataList.findMyGroups(app.user.getShortName());
                }

                dataList = splitToPages(dataList);
            }

            if (collectionName === 'students') {
                dataList = [];
                options.forEach(function (model) {
                    dataList = dataList.concat(store.students.where({groupId: model.get('groupKey')}));
                }, this);
            }
            
            return dataList;
        };

        function splitToPages (collection) {
            var tmp = collection.slice(),
                pageElems = [],
                chunk;

            while (tmp.length > 0) {
                chunk = tmp.splice(0, groupListParams.pageSize);
                pageElems.push(chunk);
            }

            groupListParams.lastPage = pageElems.length;
            app.mediator.publish('GroupList paginator: pages defined', groupListParams);

            return pageElems[groupListParams.page];
        }

        return this;
    };

})(CS, app);