'use strict';

(function (This) {
    This.GroupInfoView = Backbone.View.extend({
        tagName: 'div',
        className: 'groupInfoView',

        template: templates.groupInfoViewTpl,

        initialize: function () {
            var groupName = this.model.get('name'),
                scheduleForOneGroup = store.groupSchedule.where({groupName: groupName});
 
            this.model.on('change', this.render, this);

            this.keyDatesView = new This.GroupKeyDatesView({
                collection: scheduleForOneGroup
            });
        },

        render: function () {
            this.$el.html(this.template(this.model.toClientJSON()));
            this.$keyDatesEl = this.$el.find('.key-dates');

            this.$keyDatesEl.append(this.keyDatesView.render().el);

            return this;
        }
    });
})(CS.Groups);