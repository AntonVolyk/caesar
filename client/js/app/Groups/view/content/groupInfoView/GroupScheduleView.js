'use strict';

(function (This) {
    This.GroupScheduleView = Backbone.View.extend({
        tagName: 'div',
        className: 'schedule-container',

        template: templates.groupScheduleViewTpl,

        initialize: function () {
            var groupName = this.model.get('name'),
                scheduleForOneGroup = store.groupSchedule.where({groupName: groupName});
 
            this.weekView = new CS.Schedule.WeekView({collection: scheduleForOneGroup});
           
            this.groupKeyDatesView = new This.GroupKeyDatesView({collection: scheduleForOneGroup});
        },

        render: function () {
            this.$el.html(this.template);
            this.$weekEl = this.$el.find('.week-wrapper');
            this.$keyDatesEl = this.$el.find('.key-dates-wrapper');

            this.$weekEl.html(this.weekView.render().el);
            this.$keyDatesEl.html(this.groupKeyDatesView.render().el);

            return this;
        }
    });
})(CS.Groups);
