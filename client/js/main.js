'use strict';

var CS = {},
    app = {},
    templates = {},
    store = {},
    i = {};

System.register(CS, ['ErrorPage', 'Menu', 'GroupList', 'Groups', 'Students', 'Schedule', 'User', 'Locations', 'Messenger', 'About', 'Storage']);
System.register(app, ['mediator', 'filter', 'router', 'subRouters', 'notFound', 'user', 'userController', 'menuController', 'infoblock', 'preload', 'modal']);

$(function () {
    System.preload().then(main);

    function main () {
        app.mediator = new Mediator();
        app.filter = new CS.Filter();
        app.modal = new CS.Modal();
        app.router = new CS.Router();

        app.preload = new CS.Storage.Controller().load();
        app.userController = new CS.User.Controller();
        app.notFoundController = new CS.ErrorPage.Controller();
        app.messengerController = new CS.Messenger.Controller();
        app.locationsController = new CS.Locations.Controller();
        app.menuController  = new CS.Menu.Controller();

        Backbone.history.start({pushState: true});
    }
});
